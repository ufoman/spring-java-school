package ru.spb.epam.common.first;

/**
 * Требуется реализовать для решения первого задания.
 */
public interface ISolver {

    /**
     * https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/Task%201.1
     */
    void task1();

    /**
     * https://bitbucket.org/elefus/spring-java-school-epam-2016/wiki/Task%201.2
     */
    void task2();

    void task3();
}
